Valitor DebitCard integrates with the popular Drupal Commerce framework and
provides a payment method to accept Debitcard payments with Valitor, an icelandic
acquirer.

Requirements
--------------------------------

You must have an active SalesCloud account to use this module.
SalesCloud module available at https://drupal.org/project/salescloud

Usage
---------------------------

Once installed and connected your SalesCloud account you may accept credit card
payments.
