<?php

/**
 * @file
 * Exposes global functionality for Commerce.
 */

/**
 * Payment method callback: Settings from.
 */
function valitor_debitcard_settings_form() {
  $form = array();

  $form['no_form']['#markup'] = t('Payment method settings are configured in SalesCloud.');

  return $form;
}


/**
 * Payment method callback: submit form.
 */
function valitor_debitcard_submit_form() {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  $form = commerce_payment_credit_card_form();

  $form['credit_card']['number']['#title'] = t('Check guarantee number');
  $form['credit_card']['number']['#description'] = t('The check guarantee number displayed on the back of the card.');
  $form['credit_card']['number']['#size'] = 10;
  $form['credit_card']['number']['#maxlength'] = 10;

  $form['credit_card']['type'] = array(
    '#title' => t('Card type'),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => array(
      'Electron' => t('Electron'),
      'Maestro' => t('Maestro'),
    ),
  );

  $form['credit_card']['ssn'] = array(
    '#title' => t('Social security number'),
    '#type' => 'textfield',
    '#description' => t('The icelandic social security number associated with the debit card.'),
    '#required' => TRUE,
    '#size' => 10,
    '#maxlength' => 10,
  );

  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function valitor_debitcard_submit_form_validate($payment_method, &$pane_form, &$pane_values, $order, $form_parents = array()) {

}


/**
 * Payment method callback: submit form submit.
 */
function valitor_debitcard_submit_form_submit($payment_method, $pane_form, $pane_values, &$order, $charge) {

  $month = $pane_values['credit_card']['exp_month'];
  $year = substr($pane_values['credit_card']['exp_year'], 2);
  $card_number = $pane_values['credit_card']['number'];
  $ssn = $pane_values['credit_card']['ssn'];
  $card_type = $pane_values['credit_card']['type'];

  $data = array(
    'payment_method' => 'commerce_valitor_debetkort',
    'amount' => $charge['amount'],
    'currency_code' => $charge['currency_code'],
    'process' => TRUE,
    'details' => array(
      'card_number' => $card_number,
      'card_type' => $card_type,
      'exp_date' => $year.$month,
      'ssn' => $ssn,
    )
  );

  $response = salescloud_create('commerce_payment_transaction', $data);

  if ($response['status_code'] == 200) {
    $payment_transaction = commerce_payment_transaction_new('valitor_creditcard', $order->order_id);
    $payment_transaction->status = $response['data']['status'];
    $payment_transaction->remote_status = $response['data']['remote_status'];
    $payment_transaction->remote_id = $response['data']['remote_id'];
    $payment_transaction->message = $response['data']['message'];
    commerce_payment_transaction_save($payment_transaction);

    if ($payment_transaction->status == COMMERCE_PAYMENT_STATUS_SUCCESS) {
      return TRUE;
    }
    else {
      drupal_set_message(t($payment_transaction->message), 'error');
    }

  }
  else {
    watchdog('valitor_creditcard_fail', json_encode($response), WATCHDOG_CRITICAL);
    drupal_set_message(t('Your payment transaction failed. Please contact the site administrator!'), 'error');
  }

  return FALSE;
}